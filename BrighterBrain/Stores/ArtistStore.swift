//
//  ArtistStore.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ArtistStore: ServiceManager {
    static let shared = ArtistStore()
    
    func getArtists<T:JsonMappable>(search:String,completion:@escaping (_ success:Bool,_ list:[T]) -> Void){
         let searchURL = Constants.ARTIST_SEARCH + "&artist=\(search)"
        requestAPI1(searchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false, []);return}
            let baseResponse = BaseApiResponseArtist.init(fromJson: JSON.init(result["results"] as Any))
            completion(true,baseResponse.artistmatches?.artist as? [T] ?? [])
        }
    }
    
    func getInfo<T:JsonMappable>(mbid:String,completion:@escaping (_ success:Bool,_ list:T?) -> Void){
        let albumSearchURL = Constants.ARTIST_INFO + "&mbid=\(mbid)"
        requestAPI1(albumSearchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false,nil);return}
            let baseResponse = T.init(fromJson: JSON.init(result["artist"] as Any))
            completion(true,baseResponse)
        }
    }
}
