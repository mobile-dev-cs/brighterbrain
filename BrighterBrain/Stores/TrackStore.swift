//
//  TrackStore.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class TrackStore: ServiceManager {
    static let shared = TrackStore()
    
    func getTracks<T:JsonMappable>(search:String,completion:@escaping (_ success:Bool,_ list:[T]) -> Void){
         let searchURL = Constants.TRACK_SEARCH + "&track=\(search)"
        requestAPI1(searchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false, []);return}
            let baseResponse = BaseApiResponseTrack.init(fromJson: JSON.init(result["results"] as Any))
            completion(true,baseResponse.trackmatches?.track as? [T] ?? [])
        }
    }
    
    func getInfo<T:JsonMappable>(mbid:String,completion:@escaping (_ success:Bool,_ list:T?) -> Void){
        let albumSearchURL = Constants.TRACK_INFO + "&mbid=\(mbid)"
        requestAPI1(albumSearchURL) { (resultDict,success) in
            guard let result = resultDict else { completion(false,nil);return}
            let baseResponse = T.init(fromJson: JSON.init(result["track"] as Any))
            completion(true,baseResponse)
        }
    }
}
