//
//  ServiceManager.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//
import SystemConfiguration
import Foundation

let kBaseMessageKey: String = "message"
let kBaseStatusKey: String = "status"
let kBaseResponseKey: String = "response"
let kBaseDataKey: String = "data"

class ServiceManager: NSObject {
    
    let defaultError: String = "Some error has been occured."
    
    public static let sharedInstance = ServiceManager()
    
    var showProgress: Bool = true
    var retry: Int = 0
    var showError = true
    
}

enum ServiceResult<T,U> {
    case success(T)
    case error(U)
}

enum APIError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}

extension ServiceManager {
    
    func requestAPI1(_ url: String, completion: @escaping (_ : [String:Any]?,_ success:Bool) -> Void) {
        let url = URL(string: url)
        print("URL ",url?.absoluteString ?? "")
        if !HUD.isVisible {
           HUD.show(.progress)
         }
        let session = URLSession(configuration: URLSessionConfiguration.default)
        if let usableUrl = url {
            let task = session.dataTask(with: usableUrl, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    HUD.hide()
                if error != nil {
                   // AlertFactory.showAlert(message: error?.localizedDescription ?? "There was an error. Please try again.")
                    completion(nil,false)
                    return;
                }
                if let data = data {
                    let resultDict = self.convertToDictionary(data: data)
                    completion(resultDict,true)
                }
            }
            })
            task.resume()
        }
    }
    
    // Serialize data to dictionary
    func convertToDictionary(data: Data) -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
}


