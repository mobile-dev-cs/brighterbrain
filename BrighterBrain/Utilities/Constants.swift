//
//  Constants.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

struct Constants {
    static let API_KEY = "f7b12573779a445cd430d8096869d857"
    static let BASE_URL = "http://ws.audioscrobbler.com/2.0/"
    static let FORMAT = "json" // only json support added
    static let ALBUM_SEARCH = Constants.BASE_URL + "?method=album.search&api_key=\(Constants.API_KEY)&format=\(FORMAT)&limit=20"
    static let ARTIST_SEARCH = Constants.BASE_URL + "?method=artist.search&api_key=\(Constants.API_KEY)&format=\(FORMAT)&limit=20&page=1"
    static let TRACK_SEARCH = Constants.BASE_URL + "?method=track.search&api_key=\(Constants.API_KEY)&format=\(FORMAT)&limit=20&page=1"
    
    static let ALBUM_INFO = Constants.BASE_URL + "?method=album.getinfo&api_key=\(Constants.API_KEY)&format=\(FORMAT)"
    static let ARTIST_INFO = Constants.BASE_URL + "?method=artist.getinfo&api_key=\(Constants.API_KEY)&format=\(FORMAT)"
    static let TRACK_INFO = Constants.BASE_URL + "?method=track.getinfo&api_key=\(Constants.API_KEY)&format=\(FORMAT)"
    
    
}

