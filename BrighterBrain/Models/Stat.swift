//
//	Stat.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Stat : NSObject, NSCoding,JsonMappable{

	var listeners : String!
	var playcount : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		listeners = json["listeners"].stringValue
		playcount = json["playcount"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if listeners != nil{
			dictionary["listeners"] = listeners
		}
		if playcount != nil{
			dictionary["playcount"] = playcount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         listeners = aDecoder.decodeObject(forKey: "listeners") as? String
         playcount = aDecoder.decodeObject(forKey: "playcount") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if listeners != nil{
			aCoder.encode(listeners, forKey: "listeners")
		}
		if playcount != nil{
			aCoder.encode(playcount, forKey: "playcount")
		}

	}

}
