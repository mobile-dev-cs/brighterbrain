//
//	Track.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Track : NSObject, NSCoding,JsonMappable{
    
    var album : Album!
    var artist : Artist!
    var duration : String!
    var listeners : String!
    var mbid : String!
    var name : String!
    var playcount : String!
    var streamable : Streamable!
    var toptags : Toptag!
    var url : String!
    var track : [Track]!
    var image : [Image]!
    var wiki : Wiki!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        let albumJson = json["album"]
        if !albumJson.isEmpty{
            album = Album(fromJson: albumJson)
        }
        let artistJson = json["artist"]
        if !artistJson.isEmpty{
            artist = Artist(fromJson: artistJson)
        }
        duration = json["duration"].stringValue
        listeners = json["listeners"].stringValue
        mbid = json["mbid"].stringValue
        name = json["name"].stringValue
        playcount = json["playcount"].stringValue
        let streamableJson = json["streamable"]
        if !streamableJson.isEmpty{
            streamable = Streamable(fromJson: streamableJson)
        }
        let toptagsJson = json["toptags"]
        if !toptagsJson.isEmpty{
            toptags = Toptag(fromJson: toptagsJson)
        }
        url = json["url"].stringValue
        track = [Track]()
        let trackArray = json["track"].arrayValue
        for trackJson in trackArray{
            let value = Track(fromJson: trackJson)
            track.append(value)
        }
        image = [Image]()
        let imageArray = json["image"].arrayValue
        for imageJson in imageArray{
            let value = Image(fromJson: imageJson)
            image.append(value)
        }
        let wikiJson = json["wiki"]
        if !wikiJson.isEmpty{
            wiki = Wiki(fromJson: wikiJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if album != nil{
            dictionary["album"] = album.toDictionary()
        }
        if artist != nil{
            dictionary["artist"] = artist.toDictionary()
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if listeners != nil{
            dictionary["listeners"] = listeners
        }
        if mbid != nil{
            dictionary["mbid"] = mbid
        }
        if name != nil{
            dictionary["name"] = name
        }
        if playcount != nil{
            dictionary["playcount"] = playcount
        }
        if streamable != nil{
            dictionary["streamable"] = streamable.toDictionary()
        }
        if toptags != nil{
            dictionary["toptags"] = toptags.toDictionary()
        }
        if url != nil{
            dictionary["url"] = url
        }
        if track != nil{
            var dictionaryElements = [[String:Any]]()
            for trackElement in track {
                dictionaryElements.append(trackElement.toDictionary())
            }
            dictionary["track"] = dictionaryElements
        }
        if image != nil{
            var dictionaryElements = [[String:Any]]()
            for imageElement in image {
                dictionaryElements.append(imageElement.toDictionary())
            }
            dictionary["image"] = dictionaryElements
        }
        if wiki != nil{
            dictionary["wiki"] = wiki.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        album = aDecoder.decodeObject(forKey: "album") as? Album
        artist = aDecoder.decodeObject(forKey: "artist") as? Artist
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        listeners = aDecoder.decodeObject(forKey: "listeners") as? String
        mbid = aDecoder.decodeObject(forKey: "mbid") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        playcount = aDecoder.decodeObject(forKey: "playcount") as? String
        streamable = aDecoder.decodeObject(forKey: "streamable") as? Streamable
        toptags = aDecoder.decodeObject(forKey: "toptags") as? Toptag
        url = aDecoder.decodeObject(forKey: "url") as? String
        image = aDecoder.decodeObject(forKey: "image") as? [Image]
         wiki = aDecoder.decodeObject(forKey: "wiki") as? Wiki
        track = aDecoder.decodeObject(forKey: "track") as? [Track]

        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if album != nil{
            aCoder.encode(album, forKey: "album")
        }
        if artist != nil{
            aCoder.encode(artist, forKey: "artist")
        }
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if listeners != nil{
            aCoder.encode(listeners, forKey: "listeners")
        }
        if mbid != nil{
            aCoder.encode(mbid, forKey: "mbid")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if playcount != nil{
            aCoder.encode(playcount, forKey: "playcount")
        }
        if streamable != nil{
            aCoder.encode(streamable, forKey: "streamable")
        }
        if toptags != nil{
            aCoder.encode(toptags, forKey: "toptags")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
       
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if wiki != nil{
            aCoder.encode(wiki, forKey: "wiki")
        }
        if track != nil{
            aCoder.encode(track, forKey: "track")
        }
        
    }
    
}
