//
//	Artist.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation

class Artist : NSObject, NSCoding,JsonMappable{
    
    var image : [Image]!
    var name : String!
    var url : String!
    var bio : Bio!
    var mbid : String!
    var ontour : String!
    var similar : Similar!
    var stats : Stat!
    var streamable : String!
    var tags : Tag!
    var listeners : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        image = [Image]()
        let imageArray = json["image"].arrayValue
        for imageJson in imageArray{
            let value = Image(fromJson: imageJson)
            image.append(value)
        }
        name = json["name"].stringValue
        url = json["url"].stringValue
        let bioJson = json["bio"]
        if !bioJson.isEmpty{
            bio = Bio(fromJson: bioJson)
        }
        mbid = json["mbid"].stringValue
        ontour = json["ontour"].stringValue
        let similarJson = json["similar"]
        if !similarJson.isEmpty{
            similar = Similar(fromJson: similarJson)
        }
        let statsJson = json["stats"]
        if !statsJson.isEmpty{
            stats = Stat(fromJson: statsJson)
        }
        streamable = json["streamable"].stringValue
        let tagsJson = json["tags"]
        if !tagsJson.isEmpty{
            tags = Tag(fromJson: tagsJson)
        }
        listeners = json["listeners"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if image != nil{
            var dictionaryElements = [[String:Any]]()
            for imageElement in image {
                dictionaryElements.append(imageElement.toDictionary())
            }
            dictionary["image"] = dictionaryElements
        }
        if name != nil{
            dictionary["name"] = name
        }
        if url != nil{
            dictionary["url"] = url
        }
        if bio != nil{
            dictionary["bio"] = bio.toDictionary()
        }
        if mbid != nil{
            dictionary["mbid"] = mbid
        }
        if ontour != nil{
            dictionary["ontour"] = ontour
        }
        if similar != nil{
            dictionary["similar"] = similar.toDictionary()
        }
        if stats != nil{
            dictionary["stats"] = stats.toDictionary()
        }
        if streamable != nil{
            dictionary["streamable"] = streamable
        }
        if tags != nil{
            dictionary["tags"] = tags.toDictionary()
        }
        if streamable != nil{
            dictionary["streamable"] = streamable
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        image = aDecoder.decodeObject(forKey: "image") as? [Image]
        name = aDecoder.decodeObject(forKey: "name") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
        bio = aDecoder.decodeObject(forKey: "bio") as? Bio
        mbid = aDecoder.decodeObject(forKey: "mbid") as? String
        ontour = aDecoder.decodeObject(forKey: "ontour") as? String
        similar = aDecoder.decodeObject(forKey: "similar") as? Similar
        stats = aDecoder.decodeObject(forKey: "stats") as? Stat
        streamable = aDecoder.decodeObject(forKey: "streamable") as? String
        tags = aDecoder.decodeObject(forKey: "tags") as? Tag
        streamable = aDecoder.decodeObject(forKey: "streamable") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if bio != nil{
            aCoder.encode(bio, forKey: "bio")
        }
        if mbid != nil{
            aCoder.encode(mbid, forKey: "mbid")
        }
        if ontour != nil{
            aCoder.encode(ontour, forKey: "ontour")
        }
        if similar != nil{
            aCoder.encode(similar, forKey: "similar")
        }
        if stats != nil{
            aCoder.encode(stats, forKey: "stats")
        }
        if streamable != nil{
            aCoder.encode(streamable, forKey: "streamable")
        }
        if tags != nil{
            aCoder.encode(tags, forKey: "tags")
        }
        if streamable != nil{
            aCoder.encode(streamable, forKey: "streamable")
        }
        
    }
    
}
