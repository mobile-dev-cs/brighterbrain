//
//	Trackmatche.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Trackmatche : NSObject, NSCoding,JsonMappable{

	var track : [Track]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		track = [Track]()
		let trackArray = json["track"].arrayValue
		for trackJson in trackArray{
			let value = Track(fromJson: trackJson)
			track.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if track != nil{
			var dictionaryElements = [[String:Any]]()
			for trackElement in track {
				dictionaryElements.append(trackElement.toDictionary())
			}
			dictionary["track"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         track = aDecoder.decodeObject(forKey: "track") as? [Track]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if track != nil{
			aCoder.encode(track, forKey: "track")
		}

	}

}
