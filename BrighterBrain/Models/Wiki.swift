//
//	Wiki.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Wiki : NSObject, NSCoding{

	var content : String!
	var published : String!
	var summary : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		content = json["content"].stringValue
		published = json["published"].stringValue
		summary = json["summary"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if content != nil{
			dictionary["content"] = content
		}
		if published != nil{
			dictionary["published"] = published
		}
		if summary != nil{
			dictionary["summary"] = summary
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         content = aDecoder.decodeObject(forKey: "content") as? String
         published = aDecoder.decodeObject(forKey: "published") as? String
         summary = aDecoder.decodeObject(forKey: "summary") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if published != nil{
			aCoder.encode(published, forKey: "published")
		}
		if summary != nil{
			aCoder.encode(summary, forKey: "summary")
		}

	}

}
