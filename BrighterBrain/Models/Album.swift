//
//	Album.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation 

class Album : NSObject, NSCoding,JsonMappable{

	var artist : String!
	var image : [Image]!
	var mbid : String!
	var name : String!
	var streamable : String!
	var url : String!
    
    var listeners : String!
    var playcount : String!
    var tags : Tag!
    var tracks : Track!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		artist = json["artist"].stringValue
		image = [Image]()
		let imageArray = json["image"].arrayValue
		for imageJson in imageArray{
			let value = Image(fromJson: imageJson)
			image.append(value)
		}
		mbid = json["mbid"].stringValue
		name = json["name"].stringValue
		streamable = json["streamable"].stringValue
		url = json["url"].stringValue
        
        listeners = json["listeners"].stringValue
        playcount = json["playcount"].stringValue
        let tagsJson = json["tags"]
        if !tagsJson.isEmpty{
            tags = Tag(fromJson: tagsJson)
        }
        let tracksJson = json["tracks"]
        if !tracksJson.isEmpty{
            tracks = Track(fromJson: tracksJson)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if artist != nil{
			dictionary["artist"] = artist
		}
		if image != nil{
			var dictionaryElements = [[String:Any]]()
			for imageElement in image {
				dictionaryElements.append(imageElement.toDictionary())
			}
			dictionary["image"] = dictionaryElements
		}
		if mbid != nil{
			dictionary["mbid"] = mbid
		}
		if name != nil{
			dictionary["name"] = name
		}
		if streamable != nil{
			dictionary["streamable"] = streamable
		}
		if url != nil{
			dictionary["url"] = url
		}
        
        if listeners != nil{
            dictionary["listeners"] = listeners
        }
        
        if playcount != nil{
            dictionary["playcount"] = playcount
        }
        if tags != nil{
            dictionary["tags"] = tags.toDictionary()
        }
        if tracks != nil{
            dictionary["tracks"] = tracks.toDictionary()
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         artist = aDecoder.decodeObject(forKey: "artist") as? String
         image = aDecoder.decodeObject(forKey: "image") as? [Image]
         mbid = aDecoder.decodeObject(forKey: "mbid") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         streamable = aDecoder.decodeObject(forKey: "streamable") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
        listeners = aDecoder.decodeObject(forKey: "listeners") as? String
        playcount = aDecoder.decodeObject(forKey: "playcount") as? String
        tags = aDecoder.decodeObject(forKey: "tags") as? Tag
        tracks = aDecoder.decodeObject(forKey: "tracks") as? Track
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if artist != nil{
			aCoder.encode(artist, forKey: "artist")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if mbid != nil{
			aCoder.encode(mbid, forKey: "mbid")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if streamable != nil{
			aCoder.encode(streamable, forKey: "streamable")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
        if listeners != nil{
            aCoder.encode(listeners, forKey: "listeners")
        }
        if playcount != nil{
            aCoder.encode(playcount, forKey: "playcount")
        }
        if tags != nil{
            aCoder.encode(tags, forKey: "tags")
        }
        if tracks != nil{
            aCoder.encode(tracks, forKey: "tracks")
        }

	}

}
