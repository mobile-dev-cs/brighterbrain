//
//	Tag.swift
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import Foundation
class Tag : NSObject, NSCoding,JsonMappable{

	var name : String!
	var url : String!
	var tag : [Tag]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		name = json["name"].stringValue
		url = json["url"].stringValue
		tag = [Tag]()
		let tagArray = json["tag"].arrayValue
		for tagJson in tagArray{
			let value = Tag(fromJson: tagJson)
			tag.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if name != nil{
			dictionary["name"] = name
		}
		if url != nil{
			dictionary["url"] = url
		}
		if tag != nil{
			var dictionaryElements = [[String:Any]]()
			for tagElement in tag {
				dictionaryElements.append(tagElement.toDictionary())
			}
			dictionary["tag"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         name = aDecoder.decodeObject(forKey: "name") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
         tag = aDecoder.decodeObject(forKey: "tag") as? [Tag]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
		if tag != nil{
			aCoder.encode(tag, forKey: "tag")
		}

	}

}
