//
//  TrackCell.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class TrackCell: BaseCell {

    override var model : JsonMappable? {
        didSet{
            guard  let track = model as? Track else {
                return
            }
            mainTitleLbl?.text = track.name ?? ""
            subtitleLbl?.text = track.artist?.name ?? ""
            if let images = track.image , images.count > 0 {
                imgView.load.request(with:images[0].text)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
