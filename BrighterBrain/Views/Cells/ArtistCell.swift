//
//  ArtistCell.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ArtistCell: BaseCell {

    override var model : JsonMappable? {
        didSet{
            guard  let artist = model as? Artist else {
                return
            }
            mainTitleLbl?.text = artist.name ?? ""
            subtitleLbl?.text = artist.name ?? ""
            if let images = artist.image , images.count > 0 {
                imgView.load.request(with: images[0].text)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
