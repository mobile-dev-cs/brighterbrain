

//
//  AlbumCell.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {
    @IBOutlet weak var mainTitleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    var model:JsonMappable?
}

class AlbumCell: BaseCell {

   override var model : JsonMappable? {
        didSet{
            guard  let album = model as? Album else {
                return
            }
            mainTitleLbl?.text = album.name ?? ""
            subtitleLbl?.text = album.artist ?? ""
            if let images = album.image , images.count > 0 {
                 imgView?.load.request(with:images[0].text)
            }
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
