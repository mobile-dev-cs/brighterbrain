//
//  TabPageOption.swift
//  TabPageViewController
//
//  Created by EndouMari on 2016/02/24.
//  Copyright © 2016年 EndouMari. All rights reserved.
//

import UIKit

public enum HidesTopContentsOnSwipeType {
    case none
    case tabBar
    case navigationBar
    case all
}

public struct TabPageOption {

    public init() {}

    public var fontSize : CGFloat = 14.0
    public var currentColor = UIColor.white
    public var separatorColor = UIColor.red
    public var defaultColor = UIColor.gray
    public var tabHeight: CGFloat = 35.0
    public var tabMargin: CGFloat = 20.0
    public var tabWidth: CGFloat?
    public var currentBarHeight: CGFloat = 4.0
    public var tabBackgroundColor: UIColor = UIColor.darkGray
    public var pageBackgoundColor: UIColor = UIColor.clear
    public var isTranslucent: Bool = false
    public var hidesTopViewOnSwipeType: HidesTopContentsOnSwipeType = .none

    /// New options
     public var isTappable: Bool = true
     public var isScrollable: Bool = true
    
    internal var tabBarAlpha: CGFloat {
        return isTranslucent ? 0.95 : 1.0
    }
    internal var tabBackgroundImage: UIImage {
        return convertImage()
    }

    fileprivate func convertImage() -> UIImage {
        let rect : CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context : CGContext? = UIGraphicsGetCurrentContext()
        let backgroundColor = tabBackgroundColor.withAlphaComponent(tabBarAlpha).cgColor
        context?.setFillColor(backgroundColor)
        context?.fill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
