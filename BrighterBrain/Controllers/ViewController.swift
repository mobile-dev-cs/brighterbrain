//
//  ViewController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var controller = TabPageViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       let step1 = UIStoryboard.getViewController(type: AlbumViewController.self)
        let step2 = UIStoryboard.getViewController(type: ArtistViewController.self)
        let step3 = UIStoryboard.getViewController(type: TrackViewController.self)
        controller.tabItems = [(step1, "Albums"), (step2, "Artists"), (step3, "Tracks")]
        controller.option.tabWidth = self.view.frame.width / 3
        controller.option.tabHeight = 45
        controller.option.separatorColor = UIColor.blue
        controller.view.frame = CGRect.init(x: 0, y: 110, width: self.view.bounds.width, height: self.view.bounds.height - 110)
        controller.willMove(toParent: self)
        self.view.addSubview(controller.view)
        self.addChild(controller)
        controller.didMove(toParent: self)
        
    }

   
    
    func reloadView(text:String){
        NotificationCenter.default.post(name: Notification.dataReset, object: text)
    }
}


extension ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      let searchString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        reloadView(text: searchString ?? "")
      return true
    }
}

