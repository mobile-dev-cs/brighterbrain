//
//  ArtistDetailController.swift
//  BrighterBrain
//
//  Created by pankaj on 13/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ArtistDetailController: UIViewController {

    var model : JsonMappable?
    @IBOutlet weak var artistImgView: UIImageView!
    @IBOutlet weak var artistNameLbl: UILabel!
    @IBOutlet weak var artistInfoTxtView: UITextView!
    @IBOutlet weak var similarArtistCollectionView: UICollectionView!
    var similarArtists : [Artist] {
        guard  let model = model as? Artist  else {return []}
        return model.similar.artist
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let artist = model as? Artist else{return}
        artistNameLbl.text = artist.name ?? "Artist"
        artistInfoTxtView.text = artist.bio?.summary ?? "Summary"
        if let images = artist.image , images.count > 0 {
            artistImgView.load.request(with:images[0].text)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ArtistDetailController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarArtists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtworkCell", for: indexPath) as? ArtworkCell else{
            return UICollectionViewCell()
        }
        cell.artist = similarArtists[indexPath.row]
        return cell
    }
    
    
}
