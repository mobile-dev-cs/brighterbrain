//
//  TrackDetailController.swift
//  BrighterBrain
//
//  Created by pankaj on 13/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class TrackDetailController: UIViewController {

    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var artistNameBtn: UIButton!
    @IBOutlet weak var listenersCountLbl: UILabel!
    @IBOutlet weak var playbackCountLbl: UILabel!
    @IBOutlet weak var albumImgView: UIImageView!
    @IBOutlet weak var albumNameLbl: UILabel!
    @IBOutlet weak var bioTxtView: UITextView!
    var model : JsonMappable?
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let track = model as? Track else{return}
        trackNameLbl.text = track.name ?? "Track"
        artistNameBtn.setTitle(track.artist?.name ?? "Artist", for: .normal)
        listenersCountLbl.text = track.listeners ?? "0"
        playbackCountLbl.text = track.playcount ?? "0"
        albumNameLbl.text = track.album?.artist ?? "Album"
        bioTxtView.text = track.wiki?.content ?? "Information"
        if let images = track.album?.image , images.count > 0 {
            albumImgView?.load.request(with:images[0].text)
        }
    }
    
    @IBAction func artistBtnClicked(_ sender: UIButton) {
        guard let track = model as? Track else{return}
        self.getArtistInfo(mbid: track.artist?.mbid ?? "0")
    }
    
    func getArtistInfo(mbid:String){
        ArtistStore.shared.getInfo(mbid: mbid) { (success, artist:Artist?) in
            if success{
                let controller = UIStoryboard.getViewControllerGeneric(type: ArtistDetailController.self)
                controller.model = artist
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
