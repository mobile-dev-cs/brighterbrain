//
//  AlbumDetailController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class AlbumDetailController: UIViewController {

    @IBOutlet weak var albumLbl: UILabel!
    @IBOutlet weak var trackTblView: UITableView!
    @IBOutlet weak var artistLbl: UILabel!
    @IBOutlet weak var artistBtn: UIButton!
    var model : JsonMappable?
    var artworkImages : [Image] {
        guard  let model = model as? Album  else {return []}
        return model.image ?? []
    }
    
    var tracks : [Track] {
        guard  let model = model as? Album  else {return []}
        return model.tracks?.track ?? []
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let album = model as? Album else{return}
        albumLbl.text = album.name ?? "Album"
        artistBtn.setTitle(album.artist ?? "Artist", for: .normal)
    }
    
    @IBAction func artistBtnClicked(_ sender: UIButton) {
       AlertFactory.showAlert(message: "No mbid found for artist")
    }
    
    func getArtistInfo(mbid:String){
        ArtistStore.shared.getInfo(mbid: mbid) { (success, artist:Artist?) in
            if success{
                let controller = UIStoryboard.getViewControllerGeneric(type: ArtistDetailController.self)
                controller.model = artist
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

}

extension AlbumDetailController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artworkImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtworkCell", for: indexPath) as? ArtworkCell else{
            return UICollectionViewCell()
        }
        cell.model = artworkImages[indexPath.row]
        return cell
    }
    
    
}

extension AlbumDetailController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackCell")
        cell?.textLabel?.text = tracks[indexPath.row].name ?? "Track \(indexPath.row)"
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = tracks[indexPath.row].url else { return}
         UIApplication.shared.open(URL.init(string:url)!, options: [:], completionHandler: nil)
    }
    
   
    
}
