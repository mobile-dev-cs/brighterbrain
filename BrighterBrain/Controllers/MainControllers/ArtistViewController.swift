//
//  ArtistViewController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ArtistViewController: ResultViewController {

    override var cellClass: BaseCell.Type {
        return ArtistCell.self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self,selector: #selector(reloadViewData),name: Notification.dataReset,object: nil)
    }
    
    @objc func reloadViewData(notification: NSNotification){
        if let searchString = notification.object as? String {
            currentSearchText = searchString
            currentPage = 1
            self.getArtists(text: searchString)
        }
    }
    
    override func requestData(completion: @escaping (Bool, [JsonMappable]) -> Void) {
        getArtists(text: currentSearchText)
    }
    
    func getArtists(text:String){
        ArtistStore.shared.getArtists(search: text) { (success, artists:[Artist]) in
            if success{
                if self.currentPage == 1 {
                    self.itemsArray.removeAll()
                }
                self.itemsArray.append(contentsOf: artists)
            }
        }
    }
    
    override func didSelect(indexPath: IndexPath) {
        guard let artist = itemsArray[indexPath.row] as? Artist else{return}
        getArtistInfo(mbid: artist.mbid ?? "")
    }
    
    func getArtistInfo(mbid:String){
        ArtistStore.shared.getInfo(mbid: mbid) { (success, artist:Artist?) in
            if success{
                let controller = UIStoryboard.getViewControllerGeneric(type: ArtistDetailController.self)
                controller.model = artist
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
   

}
