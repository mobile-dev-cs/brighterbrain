
//
//  ResultViewController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    let fetchLimit = 20
    var currentPage = 1
    var currentSearchText = ""
    
    var cellClass:BaseCell.Type {
        return BaseCell.self
    }
    var reuseID : String {
        return String(describing:cellClass.self)
    }
    
    var itemsArray = [JsonMappable](){
        didSet{
             self.tableView?.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func requestData(completion:@escaping (_ success:Bool,_ list:[JsonMappable]) -> Void){
          completion(true ,[])
    }
    
    func didSelect(indexPath:IndexPath){}
    
}

extension ResultViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseID, for: indexPath) as? BaseCell else{
            return UITableViewCell()
        }
        cell.model = itemsArray[indexPath.row]
        if indexPath.row == itemsArray.count - 5{
            if itemsArray.count % fetchLimit == 0 {
                let nextPage = (itemsArray.count /  fetchLimit) + 1
                currentPage = nextPage
                self.requestData { (success, list) in
                    self.itemsArray = list
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelect(indexPath: indexPath)
    }
    
}

