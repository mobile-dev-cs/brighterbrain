//
//  TrackViewController.swift
//  BrighterBrain
//
//  Created by pankaj on 12/12/18.
//  Copyright © 2018 pankajkainthla. All rights reserved.
//

import UIKit

class TrackViewController: ResultViewController {

    
    override var cellClass: BaseCell.Type {
        return TrackCell.self
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,selector: #selector(reloadViewData),name: Notification.dataReset,object: nil)
    }
    
    @objc func reloadViewData(notification: NSNotification){
        if let searchString = notification.object as? String {
            currentSearchText = searchString
            currentPage = 1
            self.getTracks(text: searchString)
        }
    }
    
    override func requestData(completion: @escaping (Bool, [JsonMappable]) -> Void) {
        getTracks(text: currentSearchText)
    }
    
    func getTracks(text:String){
        TrackStore.shared.getTracks(search: text) { (success, tracks:[Track]) in
            if success{
                if self.currentPage == 1 {
                    self.itemsArray.removeAll()
                }
                self.itemsArray.append(contentsOf: tracks)
            }
        }
        
    }
    
    override func didSelect(indexPath: IndexPath) {
        guard let track = itemsArray[indexPath.row] as? Track else{return}
        getTrackInfo(mbid: track.mbid ?? "")
    }
    
    func getTrackInfo(mbid:String){
        TrackStore.shared.getInfo(mbid: mbid) { (success, track:Track?) in
            if success{
                let controller = UIStoryboard.getViewControllerGeneric(type: TrackDetailController.self)
                controller.model = track
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

}
