Apis used in the application:
http://ws.audioscrobbler.com/2.0/?method=album.search&api_key=f7b12573779a445cd430d8096869d857&format=json&limit=20&page=10&album=john
http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=cher&api_key=f7b12573779a445cd430d8096869d857&format=json&limit=20&page=1
http://ws.audioscrobbler.com/2.0/?method=track.search&api_key=f7b12573779a445cd430d8096869d857&format=json&track=c&limit=20&page=1
http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=f7b12573779a445cd430d8096869d857&format=json&mbid=364e35ec-623f-3ee6-afe8-9259dbf1cb24
http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&api_key=f7b12573779a445cd430d8096869d857&format=json&mbid=bfcc6d75-a6a5-4bc6-8282-47aec8531818
http://ws.audioscrobbler.com/2.0/?method=track.getinfo&api_key=f7b12573779a445cd430d8096869d857&format=json&mbid=f1a6a40f-78f5-4918-968d-f64363bae94c


Third party libraries used :
PKHUD : https://github.com/pkluz/PKHUD
ImageLoader: https://github.com/hirohisa/ImageLoaderSwift




Issues faced :
The apis returns different type of data in different apis for the same type for eg : In one api artist have more data while in other artist model is missing data which is not enough to redirect to new screen.


QA : Tested/Developed on iPhone 8 simulator only.


Demo video: https://www.dropbox.com/s/jq27wflbg41j8uc/BrighterBrainTest.mov?dl=0



